# practicum4.6

### Задание C4.6.1

Разверните на ***Server2* *Elasticsearch+Kibana*** . Настройте визуализацию логов ***Kibana* ** на ней самой. ***Log* *shipper*** используйте любой на ваш выбор.

Пришлите ментору для проверки скриншот интерфейса ***Kibana* ** с ее логами и конфиг-файл** *log* *shipper*** .

> Скриншот:

![](ScShot1.png)

> Файл конфига - filebeat.yml:

```
filebeat.inputs:
- type: filestream
  enabled: true 
  paths:
    - /var/log/kibana/kibana.log
filebeat.config.modules:
  path: ${path.config}/modules.d/*.yml
  reload.enabled: false
setup.template.settings:
  index.number_of_shards: 1
setup.kibana:
host: "localhost:5601"
output.logstash:
  hosts: ["localhost:5044"]
processors:
  - add_host_metadata:
      when.not.contains.tags: forwarded
  - add_cloud_metadata: ~
  - add_docker_metadata: ~
  - add_kubernetes_metadata: ~
```

### Задание C4.6.2

1. Настройте на ***Server1*** с помощью ***RSyslog*** отправку логов любого приложения (***nginx*** /***Jenkins*** /что-то еще на ваш выбор) в ***Elasticsearch*** на ***Server2*** .
2. Проверьте через ***Kibana*** , что логи доставляются (в пункте ***Discover*** ).

Для проверки пришлите ментору скриншот с логами вашего приложения из ***Kibana*** и конфиги самого приложения и ***RSyslog*** .

> Поднял на Server1 nginx. Сделал двумя путями. Первый путь - так, как написано в задании.
> Т.е. nginx - local log - rsyslog - remote rsyslog - filebeat - logstash - elasticsearch - kibana.
> Путь простой, т.к. вторая часть практически делалась в первой части задания.
> Второй вариант - более оптимальный и как описывают best practice.
> Nginx - remote.log -> logstash - elasticsearch - kibana
> Не стал заморачиваться с передачей логов в JSON-формате из nginx, хотя такой конфиг тоже подготовил.
> Перегонял логи в plain-text, потом распарсивал filter - grok. Интересная утилька.
> В модуле описание разворачивания ELK с ошибкой, и для старой версии. Поэтому, чтобы пройтись по описанию модуля (добавить auditd), пришлось поднимать ELK с секьюрностью. Отсюда - пароли/логины в конфигах.

#### First way

> Привожу только нужные куски конфигов.

nginx:

```
        log_format  jsonlog '{'
                        '"host": "$host",'
                        '"server_addr": "$server_addr",'
                        '"http_x_forwarded_for":"$http_x_forwarded_for",'
                        '"remote_addr":"$remote_addr",'
                        '"time_local":"$time_local",'
                        '"request_method":"$request_method",'
                        '"request_uri":"$request_uri",'
                        '"status":$status,'
                        '"body_bytes_sent":$body_bytes_sent,'
                        '"http_referer":"$http_referer",'
                        '"http_user_agent":"$http_user_agent",'
                        '"upstream_addr":"$upstream_addr",'
                        '"upstream_status":"$upstream_status",'
                        '"upstream_response_time":"$upstream_response_time",'
                        '"request_time":$request_time'
                '}';

        access_log /var/log/nginx-access.log;
        error_log /var/log/nginx-error.log;
```

rsyslog local:

```
<---skip---->
*.* @10.186.0.20:514
<---skip---->
```

Гонятся все логи по UDP на машину 10.186.0.20

rsyslog remote:

```
<---skip---->
# provides UDP syslog reception
module(load="imudp")
input(type="imudp" port="514")

# provides TCP syslog reception
module(load="imtcp")
input(type="imtcp" port="514")

$template remote-incoming-logs, "/var/log/remotelogs/%HOSTNAME%/%PROGRAMNAME%.log"
*.* ?remote-incoming-logs

###########################
#### GLOBAL DIRECTIVES ####
###########################
$AllowedSender UDP, 10.186.0.0/24
<---skip---->
```

filebeat забирает эти логи вот этим куском кода (filebeat-1.yml):

```
filebeat.inputs:
- type: log
  enabled: true
  paths: 
    - /var/log/remotelogs/templ-2/nginx_*.log
```

и отправляет в logstash вот этим куском:

```
output.logstash:
  hosts: ["localhost:5044"]
```

logstash принимает все, парсит, обрабатывает и отдает в elasticsearch:

```
input {
  beats {
    port => 5044
  }
}

filter {
   grok {
        match => { "message" => "(?:%{SYSLOGTIMESTAMP:timestamp}|%{TIMESTAMP_ISO8601:timestamp8601}) %{SYSLOGHOST:logsource} %{SYSLOGPROG:source} %{IPORHOST:clientip} - - \[%{HTTPDATE:access_time}\] \"%{WORD:method} %{URIPATHPARAM:request} HTTP/%{NUMBER:httpversion}\" %{NUMBER:response} (?:%{NUMBER:bytes}|-) %{QS:referrer} %{QS:agent}"}
           }
   mutate {
     convert => ["response", "integer"]
     convert => ["bytes", "integer"]
   }
   geoip {
     source => "clientip"
     add_tag => [ "nginx-geoip" ]
   }
   date {
     match => [ "access_time" , "dd/MMM/YYYY:HH:mm:ss Z" ]
     remove_field => [ "access_time" ]
   }
   useragent {
     source => "agent"
   }
}

output {
  elasticsearch {
    hosts => ["http://localhost:9200"]
    index => "nginx_log-%{+YYYY.MM.dd}"
    user => "logstash_internal"
    password => "oqxbcvNgNqgaJb0klw5C"
  }
}
```

#### Второй путь.

nginx отдает логи вот так:

```
access_log syslog:server=10.186.0.20:7777,tag=nginx_access_log;
```

на порту 7777 их встречает logstash, парсит, обрабатывает и отдает в elasticsearch(ngsyslog.conf):

```
input {
  udp {
    port => 7777
  }
}

filter {
        grok {
        match => { "message" => "%{MONTH:month}  %{MONTHDAY:monthday} %{TIME:time} %{SYSLOGHOST:logsource} %{SYSLOGPROG:source} %{IPORHOST:clientip} - - \[%{HTTPDATE:access_time}\] \"%{WORD:method} %{URIPATHPARAM:request} HTTP/%{NUMBER:httpversion}\" %{NUMBER:response} (?:%{NUMBER:bytes}|-) %{QS:referrer} %{QS:agent}"}
        remove_field => "message"
        }
        mutate {
         rename => { "[host]" => "[host.hostname]" }
        }
        mutate {
         convert => ["response", "integer"]
         convert => ["bytes", "integer"]
        }
        geoip {
         source => "clientip"
         add_tag => [ "nginx-geoip" ]
        }
        date {
         match => [ "access_time" , "dd/MMM/YYYY:HH:mm:ss Z" ]
         remove_field => [ "access_time" ]
        }
         useragent {
         source => "agent"
        }
        mutate {
         add_field => {"serviceName" => "nginx"}
        }

}

output {
  elasticsearch {
    hosts => ["http://localhost:9200"]
    index => "rsyslog-nginx-%{+YYYY.MM.dd}"
    user => "logstash_internal"
    password => "oqxbcvNgNqgaJb0klw5C"
  }
```

Результат - на скриншоте.

![](ScShot3.png)

![](ScShot4.png)